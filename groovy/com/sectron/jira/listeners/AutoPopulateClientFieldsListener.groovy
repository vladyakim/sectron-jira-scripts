package com.sectron.jira.workflows.listeners

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.issue.link.IssueLinkCreatedEvent
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder

IssueLinkCreatedEvent e = event
def im = ComponentAccessor.issueManager
def pIssue = e.issueLink.sourceObject
def issue = e.issueLink.destinationObject
//customfield_xxxxx this is client related custom field ID. In case those are changed in any way (eg. deleted then recreated),
//the corresponding ID must be changed within the script
def cFieldClientName = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10223")
def cFieldClientNameValue = pIssue.getCustomFieldValue(cFieldClientName)
def cFieldClientContactName = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10205")
def cFieldClientContactNameValue = pIssue.getCustomFieldValue(cFieldClientContactName)
def cFieldClientEmail = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10206")
def cFieldClientEmailValue = pIssue.getCustomFieldValue(cFieldClientEmail)
def cFieldClientPhone = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10207")
def cFieldClientPhoneValue = pIssue.getCustomFieldValue(cFieldClientPhone)

def destCFieldClientName = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10223")
def destCFieldClientNameValue = issue.getCustomFieldValue(destCFieldClientName)
def destCFieldClientContactName = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10205")
def destCFieldClientContactNameValue = issue.getCustomFieldValue(destCFieldClientContactName)
def destCFieldClientEmail = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10206")
def destCFieldClientEmailValue = issue.getCustomFieldValue(destCFieldClientEmail)
def destCFieldClientPhone = ComponentAccessor.customFieldManager.getCustomFieldObject("customfield_10207")
def destCFieldClientPhoneValue = issue.getCustomFieldValue(destCFieldClientPhone)


IssueChangeHolder changeHolder = new DefaultIssueChangeHolder()
//Below is setting client field value within the linked issue in case parent issue field holds any data and child field is empty
if(cFieldClientNameValue != null && destCFieldClientNameValue == null) {
	cFieldClientName.updateValue(null, issue, new ModifiedValue("", cFieldClientNameValue), changeHolder)
}
if(cFieldClientContactNameValue != null && destCFieldClientContactNameValue == null ) {
	cFieldClientContactName.updateValue(null, issue, new ModifiedValue("", cFieldClientContactNameValue), changeHolder)
}
if(cFieldClientEmailValue != null && destCFieldClientEmailValue == null) {
	cFieldClientEmail.updateValue(null, issue, new ModifiedValue("", cFieldClientEmailValue), changeHolder)
}
if(cFieldClientPhoneValue != null && destCFieldClientPhoneValue == null ) {
	cFieldClientPhone.updateValue(null, issue, new ModifiedValue("", cFieldClientPhoneValue), changeHolder)
}

