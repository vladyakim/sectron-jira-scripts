package com.sectron.jira.listeners

import java.util.regex.Matcher
import java.util.regex.Pattern

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.index.DefaultIndexManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.util.ImportUtils


Issue issue = issue
DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder()
def requestTypeFieldId = "customfield_10001"
def requestCustomField = ComponentAccessor.customFieldManager.getCustomFieldObject(requestTypeFieldId)
def requestTypeValue = issue.getCustomFieldValue(requestCustomField)


// If "Запитване по електронна поща" is changed in system below should be also changed. Same is valid for any other request type used below
if(issue.issueTypeObject.name == "Запитване по електронна поща"){
	def directSaleIssueType = ComponentAccessor.issueTypeSchemeManager.getIssueTypesForProject(issue.projectObject).find{it.name=="Директна продажба"}
	def techIssueType = ComponentAccessor.issueTypeSchemeManager.getIssueTypesForProject(issue.projectObject).find{it.name=="Технически проблем"}
	def businessSolutionIssueType = ComponentAccessor.issueTypeSchemeManager.getIssueTypesForProject(issue.projectObject).find{it.name=="Бизнес решение"}

	//Pattern user to search for subject prefix set by MS Exchange
	Pattern pattern = Pattern.compile("^(\\[[A-z].+?\\])")

	boolean issueChanged = false
	//Below code handles incoming mails based on the prefix set by MS Exchange
	//If any changes in SD portal. The values such as se/ebf90795-3501-42a2-ac9c-18bc6b45114a
	//must be changed accordingly. Those are combination from the Service Desk project key:
	//SELECT AO."KEY" FROM "AO_54307E_VIEWPORT" AO WHERE "NAME" = 'Център за поддръжка Сектрон' followed by /
	//and request type key that could be found with the below DB query
	//SELECT AO."KEY", AO."NAME" FROM "AO_54307E_VIEWPORTFORM" AO (use the name to identify proper key)
	if(issue.summary.startsWith("[Direct]")) {
		log.warn("Setting issue type for issue: ${issue.key} to: $directSaleIssueType.name")
		issue.setIssueTypeObject(directSaleIssueType)
		def requestType = requestCustomField.getCustomFieldType().getSingularObjectFromString("se/ebf90795-3501-42a2-ac9c-18bc6b45114a")
		requestCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(requestCustomField), requestType),changeHolder);
		log.warn("Request type for issue: ${issue.key} is changed to: $requestType")
		issueChanged = true
	} else if(issue.summary.startsWith("[TechSupport]")) {
		log.warn("Setting issue type for issue: ${issue.key} to: $techIssueType.name")
		issue.setIssueTypeObject(techIssueType)
		def requestType = requestCustomField.getCustomFieldType().getSingularObjectFromString("se/5bafba91-30f9-4d57-9b5e-5de3e02e9f6d")
		requestCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(requestCustomField), requestType),changeHolder);
		log.warn("Request type for issue: ${issue.key} is changed to: $requestType")
		issueChanged = true
	} else if(issue.summary.startsWith("[Business]")) {
		log.warn("Setting issue type for issue: ${issue.key} to: $businessSolutionIssueType.name")
		issue.setIssueTypeObject(businessSolutionIssueType)
		def requestType = requestCustomField.getCustomFieldType().getSingularObjectFromString("se/01f6212c-0cac-464f-9581-03189ff1c53d")
		requestCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(requestCustomField), requestType),changeHolder);
		log.warn("Request type for issue: ${issue.key} is changed to: $requestType")
		issueChanged = true
	} else if (issue.summary.startsWith("[Info]")) {
		issueChanged = true
	}
	//Below code changes, stores and reindexes any issue that request type was changed
	if(issueChanged) {
		Matcher typeMatcher = pattern.matcher(issue.summary);
		while (typeMatcher.find()) {
			log.warn("Issue: $issue.key - summary is changed from: $issue.summary to: ${issue.summary.substring(typeMatcher.end())}")
			issue.summary = issue.summary.substring(typeMatcher.end())

		}
		issue.store()
		boolean wasIndexing = ImportUtils.isIndexIssues()
		ImportUtils.setIndexIssues(true)
		def indexManager = ComponentAccessor.getComponent(DefaultIndexManager.class)
		indexManager.reIndex(issue)
		ImportUtils.setIndexIssues(wasIndexing)
	}
}