package com.sectron.jira.workflows

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.atlassian.crowd.embedded.impl.ImmutableUser
import com.atlassian.jira.bc.user.DefaultUserService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.exception.DataAccessException
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.index.DefaultIndexManager
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.ofbiz.OfBizDelegator
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.DelegatingApplicationUser
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.mail.Email
import com.atlassian.mail.server.SMTPMailServer


class ClearCustomerData {

	ClearCustomerData(issue){
		this.issue = issue
	}

	//defines the package used by the custom logger (to populate user_delete_requests.log). See log4j.properties notes provided
	Logger log = LoggerFactory.getLogger("com.sectron")

	//get current issue (client request for account deletion)
	IssueManager issueManager = ComponentAccessor.getIssueManager()
	Issue issue

	DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder()
	UserManager userManager = ComponentAccessor.userManager
	UserUtil userUtil = ComponentAccessor.userUtil

	def allImpactedIssues = []
	//Custom fields holding clients data that must be cleaned. IDs must be updated in case of changes in the system
	def accountDeletionFieldId = "customfield_10900"
	def clientCompanyFieldId = "customfield_10223"
	def clientNameFieldId = "customfield_10205"
	def clientEmailFieldId = "customfield_10206"
	def clientPhoneFieldId = "customfield_10207"
	def clientCompanyFieldObject = ComponentAccessor.customFieldManager.getCustomFieldObject(clientCompanyFieldId)
	def clientNameFieldObject = ComponentAccessor.customFieldManager.getCustomFieldObject(clientNameFieldId)
	def clientEmailFieldObject = ComponentAccessor.customFieldManager.getCustomFieldObject(clientEmailFieldId)
	def clientPhoneFieldObject = ComponentAccessor.customFieldManager.getCustomFieldObject(clientPhoneFieldId)

	def accountDeletionFieldObject = ComponentAccessor.customFieldManager.getCustomFieldObject(accountDeletionFieldId)
	def accountDeleletionValue = issue.getCustomFieldValue(accountDeletionFieldObject)
	def loggedUserkey = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().key
	def loggedUsername = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().username
	def loggedUserEmail = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().emailAddress
	def loggedUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
	def userDirectorySource = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().directoryId

	OfBizDelegator ofBizDelegator = ComponentAccessor.getOfBizDelegator()
	def userService = ComponentAccessor.getComponent(DefaultUserService.class)
	IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()

	//Below two methods are used to perform the actual user rename and de-activation and set username, full name and email patterns
	def ApplicationUser buildNewUser(ApplicationUser user, String newUsername) {
		ImmutableUser.Builder builder = ImmutableUser.newUser(user.directoryUser);
		builder.name(newUsername);
		builder.displayName("Изтрит потребител $newUsername");
		builder.emailAddress("deleted_user@email");
		builder.active(!user.active);
		return new DelegatingApplicationUser(user.id,user.key, builder.toUser());
	}

	def renameUser(ApplicationUser user,String newName) {
		def userService = ComponentAccessor.getComponent(DefaultUserService.class)
		def newUser  = buildNewUser(user, newName)
		def validationResult = userService.validateUpdateUser(newUser);

		if(!validationResult.isValid()) {
			def errors = validationResult.getErrorCollection().errorMessages
			log.error("Failed to rename user ${user.name} ,${errors.size()}")
		} else {
			userService.updateUser(validationResult);
			log.info("Renaming user ${user.name} to ${newUser.name} completed!")
		}
	}

	def getIssueIdFromKey(String issueKey) throws DataAccessException {
		return issueManager.getIssueObject(issueKey).id
	}

	def collectAllLinkedIssues(Long issueId) {

		allImpactedIssues.add(issueId)
		//log.error("${issueManager.getIssueObject(issueId).key}")
		issueLinkManager.getOutwardLinks(issueId).each{issueLink ->
			collectAllLinkedIssues(issueLink.getDestinationObject().id)
		}

	}

	//below method creates and sends user account deletion notification
	def sendUserNotificationAboutDeletion(String emailAddr, String subject, String body) {
		SMTPMailServer mailServer = ComponentAccessor.getMailServerManager().getDefaultSMTPMailServer()
		if (mailServer) {
			Email email = new Email(emailAddr)
			email.setFromName("Sectron Admin")
			email.setSubject(subject)
			email.setBody(body)
			mailServer.send(email)
			log.info("Account deletion notification mail sent to customer")
		} else {
			log.error("Account deletion notification was not sent see outgoing mail log.")
		}
	}

	def clearData() {

		//If account deletion confirmation tect value is changed in system, the change must also be reflected here
		//userDirectorySource == 1 condition make sure that only customers created within internal directory would be deleted
		if(accountDeleletionValue.toString().equals("Да") && userDirectorySource == 1) {
			log.warn("Starting deletion of user: $loggedUsername and remove all users activity references triggered by issue: {$issue.key}")

			def createdIssues = []
			def reportedIssues = []
			def ownIssues = []

			//Make sure that all uses reported and/or create by the user are found
			createdIssues.addAll(ofBizDelegator.findByLike("Issue", ["creator":loggedUserkey]).id)
			reportedIssues.addAll(ofBizDelegator.findByLike("Issue", ["reporter":loggedUserkey]).id)

			ownIssues = createdIssues

			//make distinct list of the two collections populated above
			reportedIssues.each{ reportedIssue ->
				if(!ownIssues.contains(reportedIssue)) {
					ownIssues.add(reportedIssue)
				}
			}


			//Identifies linked issues to any created/reported issue by user requesting account deletion
			ownIssues.each{ ownIssue -> collectAllLinkedIssues(ownIssue) }

			//set user performing the operation to admin
			def authContext = ComponentAccessor.getJiraAuthenticationContext()
			authContext.setLoggedInUser(userUtil.getUserByName("admin"))

			//clear all customer related fields in user reported/created issues and their linked issues
			//and re-index each issue so changes would be visible through JIRA UI
			allImpactedIssues.each{ impactedIssue ->
				def currIssue = issueManager.getIssueObject(impactedIssue)



				def clientCompanyFieldValue = currIssue.getCustomFieldValue(clientCompanyFieldObject)
				def clientNameFieldValue = currIssue.getCustomFieldValue(clientNameFieldObject)
				def clientEmailFieldValue = currIssue.getCustomFieldValue(clientEmailFieldObject)
				def clientPhoneFieldValue = currIssue.getCustomFieldValue(clientPhoneFieldObject)

				log.info("Client fields data for issue: ${currIssue.key}, clientCompanyFieldValue: ${clientCompanyFieldValue} | clientNameFieldValue: ${clientNameFieldValue} | clientEmailFieldValue: ${clientEmailFieldValue} | clientPhoneFieldValue:  ${clientPhoneFieldValue}")

				if(clientCompanyFieldValue != null) {
					clientCompanyFieldObject.updateValue(null, currIssue, new ModifiedValue(clientCompanyFieldValue, ""), changeHolder)
				}
				if(clientNameFieldValue != null) {
					clientNameFieldObject.updateValue(null, currIssue, new ModifiedValue(clientNameFieldValue, ""), changeHolder)
				}
				if(clientEmailFieldValue != null) {
					clientEmailFieldObject.updateValue(null, currIssue, new ModifiedValue(clientEmailFieldValue, ""), changeHolder)
				}
				if(clientPhoneFieldValue != null) {
					clientPhoneFieldObject.updateValue(null, currIssue, new ModifiedValue(clientPhoneFieldValue, ""), changeHolder)
				}

				def indexManager = ComponentAccessor.getComponent(DefaultIndexManager.class)
				indexManager.reIndex(ComponentAccessor.issueManager.getIssueObject(impactedIssue))

			}

			//Renames user by assigning new username User_ + current timestamp (thus unique username is assigned)
			Long timestamp = (Long) new Date().getTime()
			def newUsername = "User_" + timestamp //(to ensure uniqueness)


			renameUser(loggedUser, newUsername)

			//below are the subject and content of the notification mail sent to customer
			String subject = "Успешно изтриване на потребителски акаунт"
			String body = "Здравейте,\r\n\r\nС това писмо потвърждаваме успешното изтриване на вашия акаунт в Центъра за поддръжка на Сектрон.\r\n\r\nПоздрави,\r\nЕкипът на Сектрон"
			//mail is sent to customer
			sendUserNotificationAboutDeletion(loggedUserEmail,subject,body )

		}
	}
}

def cleadDataClass = new ClearCustomerData(issue)
cleadDataClass.clearData()