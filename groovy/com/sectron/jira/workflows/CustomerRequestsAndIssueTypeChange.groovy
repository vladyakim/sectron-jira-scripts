package com.sectron.jira.workflows

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.user.ApplicationUser

Issue issue = issue
DefaultIssueChangeHolder changeHolder = new DefaultIssueChangeHolder()
//customfield_xxxxx this is client related custom field ID. In case those are changed in any way (eg. deleted then recreated),
//the corresponding ID must be changed within the script
def newIssueTypeFieldId = "customfield_10800"
def requestTypeFieldId = "customfield_10001"
def newIssueTypeField = ComponentAccessor.customFieldManager.getCustomFieldObject(newIssueTypeFieldId)
def newIssueTypeFieldValue = issue.getCustomFieldValue(newIssueTypeField)
def requestCustomField = ComponentAccessor.customFieldManager.getCustomFieldObject(requestTypeFieldId)

log.warn("Issue type: ${issue.issueType.name}, update with new issue type selected: ${newIssueTypeFieldValue} ")


//Code that executes in case that new issue/request type selected is different than the original one
//names used below must be changed accordingly if those are renamed/changed within the system
if(!newIssueTypeFieldValue.toString().equals(issue.issueType.name.toString())){
	def directSaleIssueType = ComponentAccessor.issueTypeSchemeManager.getIssueTypesForProject(issue.projectObject).find{it.name=="Директна продажба"}
	def techIssueType = ComponentAccessor.issueTypeSchemeManager.getIssueTypesForProject(issue.projectObject).find{it.name=="Технически проблем"}
	def businessSolutionIssueType = ComponentAccessor.issueTypeSchemeManager.getIssueTypesForProject(issue.projectObject).find{it.name=="Бизнес решение"}
	def emailRequestIssueType = ComponentAccessor.issueTypeSchemeManager.getIssueTypesForProject(issue.projectObject).find{it.name=="Запитване по електронна поща"}

	//If any changes in SD portal. The values such as se/ebf90795-3501-42a2-ac9c-18bc6b45114a
	//must be changed accordingly. Those are combination from the Service Desk project key:
	//SELECT AO."KEY" FROM "AO_54307E_VIEWPORT" AO WHERE "NAME" = 'Център за поддръжка Сектрон' followed by /
	//and request type key that could be found with the below DB query
	//SELECT AO."KEY", AO."NAME" FROM "AO_54307E_VIEWPORTFORM" AO (use the name to identify proper key)
	if(newIssueTypeFieldValue.toString().equals(techIssueType.name.toString())){
		log.warn("Setting issue type for issue: ${issue.key} to: $techIssueType.name")
		issue.setIssueTypeObject(techIssueType)
		def requestType = requestCustomField.getCustomFieldType().getSingularObjectFromString("se/5bafba91-30f9-4d57-9b5e-5de3e02e9f6d")
		requestCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(requestCustomField), requestType),changeHolder);
		log.warn("Request type for issue: ${issue.key} is changed to: $requestType")
	} else if (newIssueTypeFieldValue.toString().equals(directSaleIssueType.name.toString())){
		log.warn("Setting issue type for issue: ${issue.key} to: $directSaleIssueType.name")
		issue.setIssueTypeObject(directSaleIssueType)
		def requestType = requestCustomField.getCustomFieldType().getSingularObjectFromString("se/ebf90795-3501-42a2-ac9c-18bc6b45114a")
		requestCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(requestCustomField), requestType),changeHolder);
		log.warn("Request type for issue: ${issue.key} is changed to: $requestType")
	} else if(newIssueTypeFieldValue.toString().equals(businessSolutionIssueType.name.toString())){
		log.warn("Setting issue type for issue: ${issue.key} to: $businessSolutionIssueType.name")
		issue.setIssueTypeObject(businessSolutionIssueType)
		def requestType = requestCustomField.getCustomFieldType().getSingularObjectFromString("se/01f6212c-0cac-464f-9581-03189ff1c53d")
		requestCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(requestCustomField), requestType),changeHolder);
		log.warn("Request type for issue: ${issue.key} is changed to: $requestType")
	} else if (newIssueTypeFieldValue.toString().equals(emailRequestIssueType.name.toString())){
		log.warn("Setting issue type for issue: ${issue.key} to: $emailRequestIssueType.name")
		issue.setIssueTypeObject(emailRequestIssueType)
		def requestType = requestCustomField.getCustomFieldType().getSingularObjectFromString("se/2e6c159d-fad5-4874-9e5f-e6106ff8091c")
		requestCustomField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(requestCustomField), requestType),changeHolder);
		log.warn("Request type for issue: ${issue.key} is changed to: $requestType")
	}
	//below 2 lines clear the assignee and store the issue
	issue.assigneeId = null
	issue.store()

	//below code triggers "Issue moved" event to be processed by listeners -> used in automation rules and notification schemes
	Long eventId = ComponentAccessor.getEventTypeManager().eventTypes.find{it.name=='Issue Moved'}.id

	ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
	def issueEventManager = ComponentAccessor.getIssueEventManager()

	issueEventManager.dispatchEvent(eventId, issue, user, false)
}