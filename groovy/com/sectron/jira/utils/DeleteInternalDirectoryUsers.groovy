package groovy.com.sectron.jira.utils

import com.atlassian.jira.component.ComponentAccessor

def userManager = ComponentAccessor.getUserManager()
def userUtil = ComponentAccessor.getUserUtil()
def crowd = ComponentAccessor.getCrowdService()
def myUser = ComponentAccessor.jiraAuthenticationContext.getUser()
def allAppUsernames = userManager.getAllApplicationUsers().collect{it.username}
int count = 0
allAppUsernames.each{ appUser ->
	//get user directory ID, the internal dir is always 1
	def directoryId = crowd.getUser(appUser).directoryId
	def email = crowd.getUser(appUser).emailAddress
	if(directoryId == 1 && !email.endsWith("botronsoft.com") && !appUser.equals("admin")  && !email.equals("deleted_user@email")) {
		//below removes all users for whom above rule applies
		userUtil.removeUser(myUser, userManager.getUser(appUser))
		count++
	}
}
log.warn("$count users deleted")